const expect = require('unexpected').clone();
const {
  describe,
  it
} = require('mocha');

// Ruby already has flatMap, so we borrow that from ramda
const flat_map = require('ramda').chain;

describe('Many', () => {
  it('gets all words from a blog posts page', () => {
    const blogs = [{
      categories: [{
        posts: [{
            comments: ['I love cats', 'I love dogs']
          },
          {
            comments: ['I love mice', 'I love pigs']
          }
        ]
      }, {
        posts: [{
            comments: ['I hate cats', 'I hate dogs']
          },
          {
            comments: ['I hate mice', 'I hate pigs']
          }
        ]

      }]
    }, {
      categories: [{
        posts: [{
          comments: ['Red is better than blue']
        }, {
          comments: ['Blue is better than red']
        }]
      }]
    }];

    Array.prototype.flat_map = function(fn) {
      return flat_map(fn, this);
    };

    const words_in = blogs =>
      blogs.flat_map(blog =>
        blog.categories.flat_map(category =>
          category.posts.flat_map(post =>
            post.comments.flat_map(comment =>
              comment.split(/\s+/)))));

    expect(words_in(blogs), 'to equal', ["I", "love", "cats", "I", "love", "dogs", "I",
      "love", "mice", "I", "love", "pigs", "I",
      "hate", "cats", "I", "hate", "dogs", "I",
      "hate", "mice", "I", "hate", "pigs", "Red",
      "is", "better", "than", "blue", "Blue", "is",
      "better", "than", "red"
    ]);
  });
});
